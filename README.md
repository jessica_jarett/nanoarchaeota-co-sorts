## Supporting data for: 

# Single-cell genomics of co-sorted Nanoarchaeota suggests novel putative host associations and diversification of proteins involved in symbiosis

*Jessica K. Jarett, Stephen Nayfach, Mircea Podar, William Inskeep, Natalia N. Ivanova, Jacob Munson-McGee, Frederik Schulz, Mark Young, Zackary J. Jay, Jacob P. Beam, Nikos Kyrpides, Rex R. Malmstrom, Ramunas Stepanauskas, Tanja Woyke*

This repository contains sequence data, genome bins, and other supporting data as described in the paper. Please contact Jessica Jarett (jkjarett@lbl.gov) for questions regarding the data and their use.

Read data and assemblies of SAGs and references are also available through the JGI [Nano co-sorts portal](https://genome.jgi.doe.gov/portal/Nano_cosorts/Nano_cosorts.home.html)  

If you use this resource, please cite: <link to article.>

___________________________________________________________________________________________________________
###File descriptions


**16S rRNA:**  
16S rRNA sequences (aligned, aligned and masked) and phylogeny.

**IMG reference genomes:**  
Fasta files of reference genomes not available through NCBI. These are also available in [IMG](https://img.jgi.doe.gov/) using the IMG taxon ids listed in Additional file 2, Table S2.

**Nanoarchaeota Clade 1 reference:**  
All IMG files (annotations, fasta, genes, etc) for AB-777-F03, the reference genome for Clade 1 Nanoarchaeota.

**Nanoarchaeota SAGs**  
Complete fasta files of Nanoarchaeota SAGs and co-sorted SAGs.

**SAG genome bins**  
Genome bins of Nanoarchaeota and putative hosts, as well as unbinned contigs, from Nanoarchaeota SAGs.

**SNPs**

* Clade 1: SNP data in raw format and per-gene, grouped by functional category. Also provided are amino acid alignments of the two proteins discussed in detail, a cytochrome bd-I ubiquinol oxidase and a Type 2 secretion system protein F (FlaJ/TadC homologue).
* intra-SAG: SNP data for mapping of reads to individual SAG assemblies, script for statistical testing and resulting p values.
* pooled SAGs: SNP data for simulation estimating expected diversity of multiple Nanoarchaeota on a single host cell, and script for creating figure shown in manuscript.

**Ribosomal proteins**  
HMMs used to extract ribosomal protein sequences, concatenated alignment, ribosomal protein phylogeny, and associated output files.
