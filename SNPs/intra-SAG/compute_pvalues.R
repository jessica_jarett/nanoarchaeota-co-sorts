

data <- read.csv('count_snps.tsv', sep=' ', stringsAsFactors=F, header=T)
names(data) <- c('taxon_id', 'type', 'sort', 'length', 'min_depth', 'sites', 'min_maf', 'snps')
data$group <- paste(data$type, data$sort)
data$snps_kb <- 1000*data$snps/data$sites

res <- as.data.frame(matrix(nrow=0, ncol=0))

for (min_depth in unique(data$min_depth)){

	for (min_maf in unique(data$min_maf)){

		min_sites <- 10000

		x <- data[data$min_maf==min_maf & data$min_depth==min_depth & data$sites>=min_sites,]
		
		x1 <- x[x$group=='nano co-sort','snps_kb']
		x2 <- x[x$group=='nano single','snps_kb']
		x3 <- x[x$group=='host co-sort','snps_kb']
		
		row <- nrow(res)+1
		res[row, 'min_depth'] <- min_depth
		res[row, 'min_maf'] <- min_maf
		res[row, 'min_sites'] <- min_sites
		res[row, 'n_cosort'] <- length(x1)
		res[row, 'n_single'] <- length(x2)
		res[row, 'n_host'] <- length(x3)
		res[row, 'avg_cosort'] <- mean(x1)
		res[row, 'avg_single'] <- mean(x2)
		res[row, 'avg_host'] <- mean(x3)
		res[row, 'median_cosort'] <- median(x1)
		res[row, 'median_single'] <- median(x2)
		res[row, 'median_host'] <- median(x3)
		res[row, 'p_cosort_vs_single'] <- wilcox.test(x1, x2, alternative="greater")$p.value
		res[row, 'p_cosort_vs_host'] <- wilcox.test(x1, x3, alternative="greater")$p.value	
		
	}

}

write.table(res, 'pvalues.tsv', row.names=F, quote=F, sep='\t')
